<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!is_null($_POST['cerrarSesion'])) {
        session_unset();
    }
}
const AllowInclude = TRUE;
?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css?v=1.1">

    <title>TriviSiette | Inicio</title>
    <meta name="description" content="Página de inicio de TriviSiette.">
    <meta name="author" content="Eloy Barrionuevo Jiménez">

    <script src="https://unpkg.com/pencil.js"></script>
    <!-- Scripts Boostrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- CSS Boostrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>
<!-- Menú -->
<nav class="navbar navbar-expand-md navbar-dark bg-skyblue">
    <a class="navbar-brand" href="index.php">TriviSiette</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="index.php">Inicio</a>
            <?php if (isset($_SESSION['user'])) { ?> <a class="nav-item nav-link" href="mytests.php">Mis tests</a> <?php } ?>
        </div>

    </div>
    <?php if (isset($_SESSION['user'])) { ?>
        <form method="post" action="index.php">
            <input name="cerrarSesion" type="hidden" value="">
            <button class="btn btn-sm btn-danger" type="submit">Cerrar Sesión</button>
        </form>
        <?php
    } else { ?>
        <div class="row justify-content-center">
            <a class="btn btn-info" href="login.php">Iniciar Sesión</a>
            <div class="mr-2"></div>
            <a class="btn btn-outline-dark mr-3" href="signup.php">Registrarse</a>
        </div>
    <?php } ?>
</nav>

<div class="container mt-5">
    <h1>Bienvenido a TriviSiette</h1>
    <h4 class="mt-4">En TriviSiette podrás crear o unirte a concursos de preguntas utilizando los servicios de Siette. Para crear un concurso, empieza creando un test en Siette e invita a los concursantes
        compartiendo el código.</h4>
    <p></p>
    <h5>[DEBUG]</h5>

</div>

<script>
    drawBoard();

    function rotate(cx, cy, x, y, angle) {
        var radians = (Math.PI / 180) * angle,
            cos = Math.cos(radians),
            sin = Math.sin(radians),
            nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
            ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
        return [nx, ny];
    }


    function drawBoard() {
        const scene = new Pencil.Scene(undefined, {
            fill: "#33586A",
        });

        // White background
        const center = [scene.width / 2, scene.height / 2]

        const relation = 30 / 160;
        const relation2 = 32 / 160;

        var scale = scene.width;

        if (scene.width > scene.height) {
            scale = scene.height;
        }

        const outerRadius = scale * 0.48;
        const innerRadius = outerRadius - outerRadius*relation;

        const outerCircle = new Pencil.Circle(center, outerRadius, {
            fill: "#FFFFFF",
        });

        const innerCircle = new Pencil.Circle(center, innerRadius, {
            fill: "#33586A",
        });
        const hexagoneRadius = outerRadius*relation2;

        const hexagone = new Pencil.RegularPolygon(center, 6, hexagoneRadius*0.9, {
            fill: "#33586A",
        });
        const rectangleWidth = innerRadius*2.2;
        const rectangleHeight = hexagoneRadius;
        const centerHorizontalRectangle = [center[0] - rectangleWidth/2, center[1] - rectangleHeight/2];

        const horizontalRectangle = new Pencil.Rectangle(centerHorizontalRectangle, rectangleWidth, rectangleHeight, {
            fill: "#FFFFFF",
        });

        const centerDiagonalRectangle = rotate(center[0],center[1],centerHorizontalRectangle[0],centerHorizontalRectangle[1], 300);
        const diagonalRectangle = new Pencil.Rectangle(centerDiagonalRectangle, rectangleWidth, rectangleHeight, {
            fill: "#FFFFFF",
            rotation: 0.166666,
        });
        const centerDiagonalRectangle2 = rotate(center[0],center[1],centerHorizontalRectangle[0],centerHorizontalRectangle[1], 60);
        const diagonalRectangle2 = new Pencil.Rectangle(centerDiagonalRectangle2, rectangleWidth, rectangleHeight, {
            fill: "#FFFFFF",
            rotation: 0.833333,
        });

        // Color boxes
        // Green #4DAB54
        // Blue #61C0C4
        // Orange #F17B33
        // Pink #F985B9
        // Yellow #EFDB3B
        // Brown #B06932
        const boxHeight = hexagoneRadius*0.9;
        const boxWidth = boxHeight * 22/33;
        const unit = outerRadius/360;

        // Left horizontal
        const centerGreenRectangle = [center[0] - unit*103, center[1] - unit*32];

        const greenRectangle = new Pencil.Rectangle(centerGreenRectangle, boxWidth, boxHeight, {
            fill: "#4DAB54",
        });

        const centerBlueRectangle = [center[0] - unit*150, center[1] - unit*32];
        const blueRectangle = new Pencil.Rectangle(centerBlueRectangle, boxWidth, boxHeight, {
            fill: "#61C0C4",
        });

        const centerOrangeRectangle = [center[0] - unit*197, center[1] - unit*32];
        const orangeRectangle = new Pencil.Rectangle(centerOrangeRectangle, boxWidth, boxHeight, {
            fill: "#F17B33",
        });

        const centerPinkRectangle = [center[0] - unit*244, center[1] - unit*32];
        const pinkRectangle = new Pencil.Rectangle(centerPinkRectangle, boxWidth, boxHeight, {
            fill: "#F985B9",
        });

        const centerYellowRectangle = [center[0] - unit*291, center[1] - unit*32];
        const yellowRectangle = new Pencil.Rectangle(centerYellowRectangle, boxWidth, boxHeight, {
            fill: "#EFDB3B",
        });


        // Right horizontal
        const centerOrangeRectangle2 = [center[0] + unit*60, center[1] - unit*32];
        const orangeRectangle2 = new Pencil.Rectangle(centerOrangeRectangle2, boxWidth, boxHeight, {
            fill: "#F17B33",
        });

        const centerPinkRectangle2 = [center[0] + unit*107, center[1] - unit*32];
        const pinkRectangle2 = new Pencil.Rectangle(centerPinkRectangle2, boxWidth, boxHeight, {
            fill: "#F985B9",
        });

        const centerGreenRectangle2 = [center[0] + unit*154, center[1] - unit*32];
        const greenRectangle2 = new Pencil.Rectangle(centerGreenRectangle2, boxWidth, boxHeight, {
            fill: "#4DAB54",
        });

        const centerBlueRectangle2 = [center[0] + unit*201, center[1] - unit*32];
        const blueRectangle2 = new Pencil.Rectangle(centerBlueRectangle2, boxWidth, boxHeight, {
            fill: "#61C0C4",
        });

        const centerBrownRectangle = [center[0] + unit*248, center[1] - unit*32];
        const brownRectangle = new Pencil.Rectangle(centerBrownRectangle, boxWidth, boxHeight, {
            fill: "#B06932",
        });

        // Upper diagonal1
        var centerBrownRectangle2 = [center[0] - unit*103, center[1] - unit*32];
        centerBrownRectangle2 = rotate(center[0],center[1],centerBrownRectangle2[0],centerBrownRectangle2[1], 300);

        const brownRectangle2 = new Pencil.Rectangle(centerBrownRectangle2, boxWidth, boxHeight, {
            fill: "#B06932",
            rotation: 0.166666,
        });

        var centerOrangeRectangle3 = [center[0] - unit*150, center[1] - unit*32];
        centerOrangeRectangle3 = rotate(center[0],center[1],centerOrangeRectangle3[0],centerOrangeRectangle3[1], 300);
        const orangeRectangle3 = new Pencil.Rectangle(centerOrangeRectangle3, boxWidth, boxHeight, {
            fill: "#F17B33",
            rotation: 0.166666,
        });

        var centerYellowRectangle2 = [center[0] - unit*197, center[1] - unit*32];
        centerYellowRectangle2 = rotate(center[0],center[1],centerYellowRectangle2[0],centerYellowRectangle2[1], 300);
        const yellowRectangle2 = new Pencil.Rectangle(centerYellowRectangle2, boxWidth, boxHeight, {
            fill: "#EFDB3B",
            rotation: 0.166666,
        });

        var centerGreenRectangle3 = [center[0] - unit*244, center[1] - unit*32];
        centerGreenRectangle3 = rotate(center[0],center[1],centerGreenRectangle3[0],centerGreenRectangle3[1], 300);
        const greenRectangle3 = new Pencil.Rectangle(centerGreenRectangle3, boxWidth, boxHeight, {
            fill: "#4DAB54",
            rotation: 0.166666,
        });

        var centerPinkRectangle3 = [center[0] - unit*291, center[1] - unit*32];
        centerPinkRectangle3 = rotate(center[0],center[1],centerPinkRectangle3[0],centerPinkRectangle3[1], 300);
        const pinkRectangle3 = new Pencil.Rectangle(centerPinkRectangle3, boxWidth, boxHeight, {
            fill: "#F985B9",
            rotation: 0.166666,
        });

        // Bottom diagonal1
        var centerYellowRectangle3 = [center[0] + unit*60, center[1] - unit*32];
        centerYellowRectangle3 = rotate(center[0],center[1],centerYellowRectangle3[0],centerYellowRectangle3[1], 300);
        const yellowRectangle3 = new Pencil.Rectangle(centerYellowRectangle3, boxWidth, boxHeight, {
            fill: "#EFDB3B",
            rotation: 0.166666,
        });

        var centerGreenRectangle4 = [center[0] + unit*107, center[1] - unit*32];
        centerGreenRectangle4 = rotate(center[0],center[1],centerGreenRectangle4[0],centerGreenRectangle4[1], 300);
        const greenRectangle4 = new Pencil.Rectangle(centerGreenRectangle4, boxWidth, boxHeight, {
            fill: "#4DAB54",
            rotation: 0.166666,
        });

        var centerBrownRectangle3 = [center[0] + unit*154, center[1] - unit*32];
        centerBrownRectangle3 = rotate(center[0],center[1],centerBrownRectangle3[0],centerBrownRectangle3[1], 300);
        const brownRectangle3 = new Pencil.Rectangle(centerBrownRectangle3, boxWidth, boxHeight, {
            fill: "#B06932",
            rotation: 0.166666,
        });

        var centerOrangeRectangle4 = [center[0] + unit*201, center[1] - unit*32];
        centerOrangeRectangle4 = rotate(center[0],center[1],centerOrangeRectangle4[0],centerOrangeRectangle4[1], 300);
        const orangeRectangle4 = new Pencil.Rectangle(centerOrangeRectangle4, boxWidth, boxHeight, {
            fill: "#F17B33",
            rotation: 0.166666,
        });

        var centerBlueRectangle3 = [center[0] + unit*248, center[1] - unit*32];
        centerBlueRectangle3 = rotate(center[0],center[1],centerBlueRectangle3[0],centerBlueRectangle3[1], 300);
        const blueRectangle3 = new Pencil.Rectangle(centerBlueRectangle3, boxWidth, boxHeight, {
            fill: "#61C0C4",
            rotation: 0.166666,
        });

        // Bottom diagonal2
        var centerPinkRectangle4 = [center[0] - unit*103, center[1] - unit*32];
        centerPinkRectangle4 = rotate(center[0],center[1],centerPinkRectangle4[0],centerPinkRectangle4[1], 60);
        const pinkRectangle4 = new Pencil.Rectangle(centerPinkRectangle4, boxWidth, boxHeight, {
            fill: "#F985B9",
            rotation: 0.833333,
        });

        var centerBrownRectangle4 = [center[0] - unit*150, center[1] - unit*32];
        centerBrownRectangle4 = rotate(center[0],center[1],centerBrownRectangle4[0],centerBrownRectangle4[1], 60);
        const brownRectangle4 = new Pencil.Rectangle(centerBrownRectangle4, boxWidth, boxHeight, {
            fill: "#B06932",
            rotation: 0.833333,
        });

        var centerBlueRectangle4 = [center[0] - unit*197, center[1] - unit*32];
        centerBlueRectangle4 = rotate(center[0],center[1],centerBlueRectangle4[0],centerBlueRectangle4[1], 60);
        const blueRectangle4 = new Pencil.Rectangle(centerBlueRectangle4, boxWidth, boxHeight, {
            fill: "#61C0C4",
            rotation: 0.833333,
        });

        var centerYellowRectangle4 = [center[0] - unit*244, center[1] - unit*32];
        centerYellowRectangle4 = rotate(center[0],center[1],centerYellowRectangle4[0],centerYellowRectangle4[1], 60);
        const yellowRectangle4 = new Pencil.Rectangle(centerYellowRectangle4, boxWidth, boxHeight, {
            fill: "#EFDB3B",
            rotation: 0.833333,
        });

        var centerOrangeRectangle5 = [center[0] - unit*291, center[1] - unit*32];
        centerOrangeRectangle5 = rotate(center[0],center[1],centerOrangeRectangle5[0],centerOrangeRectangle5[1], 60);
        const orangeRectangle5 = new Pencil.Rectangle(centerOrangeRectangle5, boxWidth, boxHeight, {
            fill: "#F17B33",
            rotation: 0.833333,
        });

        // Upper diagonal2
        var centerBlueRectangle5 = [center[0] + unit*60, center[1] - unit*32];
        centerBlueRectangle5 = rotate(center[0],center[1],centerBlueRectangle5[0],centerBlueRectangle5[1], 60);
        const blueRectangle5 = new Pencil.Rectangle(centerBlueRectangle5, boxWidth, boxHeight, {
            fill: "#61C0C4",
            rotation: 0.833333,
        });

        var centerYellowRectangle5 = [center[0] + unit*107, center[1] - unit*32];
        centerYellowRectangle5 = rotate(center[0],center[1],centerYellowRectangle5[0],centerYellowRectangle5[1], 60);
        const yellowRectangle5 = new Pencil.Rectangle(centerYellowRectangle5, boxWidth, boxHeight, {
            fill: "#EFDB3B",
            rotation: 0.833333,
        });

        var centerPinkRectangle5 = [center[0] + unit*154, center[1] - unit*32];
        centerPinkRectangle5 = rotate(center[0],center[1],centerPinkRectangle5[0],centerPinkRectangle5[1], 60);
        const pinkRectangle5 = new Pencil.Rectangle(centerPinkRectangle5, boxWidth, boxHeight, {
            fill: "#F985B9",
            rotation: 0.833333,
        });

        var centerBrownRectangle5 = [center[0] + unit*201, center[1] - unit*32];
        centerBrownRectangle5 = rotate(center[0],center[1],centerBrownRectangle5[0],centerBrownRectangle5[1], 60);
        const brownRectangle5 = new Pencil.Rectangle(centerBrownRectangle5, boxWidth, boxHeight, {
            fill: "#B06932",
            rotation: 0.833333,
        });

        var centerGreenRectangle5 = [center[0] + unit*248, center[1] - unit*32];
        centerGreenRectangle5 = rotate(center[0],center[1],centerGreenRectangle5[0],centerGreenRectangle5[1], 60);
        const greenRectangle5 = new Pencil.Rectangle(centerGreenRectangle5, boxWidth, boxHeight, {
            fill: "#4DAB54",
            rotation: 0.833333,
        });

        // Cheeses
        const centerBrownCheeseRectangle = [center[0] - unit*355, center[1] - unit*32];
        const brownCheeseRectangle = new Pencil.Rectangle(centerBrownCheeseRectangle, boxWidth + 17*unit, boxHeight, {
            fill: "#B06932",
        });

        const centerYellowCheeseRectangle = [center[0] + unit*295, center[1] - unit*32];
        const yellowCheeseRectangle = new Pencil.Rectangle(centerYellowCheeseRectangle, boxWidth + 17*unit, boxHeight, {
            fill: "#EFDB3B",
        });

        var centerPinkCheeseRectangle = [center[0] + unit*295, center[1] - unit*32];
        centerPinkCheeseRectangle = rotate(center[0],center[1],centerPinkCheeseRectangle[0],centerPinkCheeseRectangle[1], 300);
        const pinkCheeseRectangle = new Pencil.Rectangle(centerPinkCheeseRectangle, boxWidth + 17*unit, boxHeight, {
            fill: "#F985B9",
            rotation: 0.166666,
        });

        var centerBlueCheeseRectangle = [center[0] - unit*355, center[1] - unit*32];
        centerBlueCheeseRectangle = rotate(center[0],center[1],centerBlueCheeseRectangle[0],centerBlueCheeseRectangle[1], 300);
        const blueCheeseRectangle = new Pencil.Rectangle(centerBlueCheeseRectangle, boxWidth + 17*unit, boxHeight, {
            fill: "#61C0C4",
            rotation: 0.166666,
        });

        var centerGreenCheeseRectangle = [center[0] - unit*355, center[1] - unit*32];
        centerGreenCheeseRectangle = rotate(center[0],center[1],centerGreenCheeseRectangle[0],centerGreenCheeseRectangle[1], 60);
        const greenCheeseRectangle = new Pencil.Rectangle(centerGreenCheeseRectangle, boxWidth + 17*unit, boxHeight, {
            fill: "#4DAB54",
            rotation: 0.833333,
        });

        var centerOrangeCheeseRectangle = [center[0] + unit*295, center[1] - unit*32];
        centerOrangeCheeseRectangle = rotate(center[0],center[1],centerOrangeCheeseRectangle[0],centerOrangeCheeseRectangle[1], 60);
        const orangeCheeseRectangle = new Pencil.Rectangle(centerOrangeCheeseRectangle, boxWidth + 17*unit, boxHeight, {
            fill: "#F17B33",
            rotation: 0.833333,
        });

        // Circle
        var centerYellowRectangle6 = [center[0] - unit*355, center[1] - unit*32];
        centerYellowRectangle6 = rotate(center[0],center[1],centerYellowRectangle6[0],centerYellowRectangle6[1], 360 - 60/7);
        const yellowRectangle6 = new Pencil.Rectangle(centerYellowRectangle6, boxWidth + 15*unit, boxWidth - 3*unit, {
            fill: "#EFDB3B",
            rotation: 0.166666/7,

        });

        var centerGrayRectangle = [center[0] - unit*355, center[1] - unit*32];
        centerGrayRectangle = rotate(center[0],center[1],centerGrayRectangle[0],centerGrayRectangle[1], 360 - 2*60/7);
        const grayRectangle = new Pencil.Rectangle(centerGrayRectangle, boxWidth + 15*unit, boxWidth - 3*unit, {
            fill: "#DDDDDD",
            rotation: 2 * 0.166666/7,

        });

        var centerOrangeRectangle6 = [center[0] - unit*355, center[1] - unit*32];
        centerOrangeRectangle6 = rotate(center[0],center[1],centerOrangeRectangle6[0],centerOrangeRectangle6[1], 360 - 3*60/7);
        const orangeRectangle6 = new Pencil.Rectangle(centerOrangeRectangle6, boxWidth + 15*unit, boxWidth - 3*unit, {
            fill: "#F17B33",
            rotation: 3 * 0.166666/7,

        });

        scene.add(outerCircle, innerCircle, horizontalRectangle, diagonalRectangle, diagonalRectangle2, hexagone, greenRectangle, blueRectangle, orangeRectangle, pinkRectangle, yellowRectangle,
        orangeRectangle2, pinkRectangle2, greenRectangle2, blueRectangle2, brownRectangle, brownCheeseRectangle, yellowCheeseRectangle, brownRectangle2, orangeRectangle3, yellowRectangle2,
            greenRectangle3, pinkRectangle3, yellowRectangle3, blueRectangle3, orangeRectangle4, brownRectangle3, greenRectangle4, pinkRectangle4, orangeRectangle5, yellowRectangle4,
            blueRectangle4, brownRectangle4, greenRectangle5, brownRectangle5, pinkRectangle5, yellowRectangle5, blueRectangle5, pinkCheeseRectangle, blueCheeseRectangle, greenCheeseRectangle,
            orangeCheeseRectangle, yellowRectangle6, grayRectangle, orangeRectangle6);
        scene.startLoop();
    }



</script>
</body>
</html>
